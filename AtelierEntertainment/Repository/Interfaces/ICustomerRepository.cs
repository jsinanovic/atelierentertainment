﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using domain = AtelierEntertainment.Domain;

namespace AtelierEntertainment.Database.Repository.Interfaces
{
    public interface ICustomerRepository
    {
        void Create(domain.Customer customer);
    }
}
