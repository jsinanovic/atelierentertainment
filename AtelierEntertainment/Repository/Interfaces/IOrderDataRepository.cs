﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using domain = AtelierEntertainment.Domain;

namespace AtelierEntertainment.Database.Repository.Interfaces
{
    public interface IOrderDataRepository
    {
        domain.Order CreateOrder(domain.Order order);
        ICollection<domain.Order> GetByCustomerId(long customerId);
    }
}
