﻿
using System;
using AtelierEntertainment.Database.DataAccess;
using AtelierEntertainment.Database.Repository.Interfaces;
using System.Data.SqlClient;
using domain = AtelierEntertainment.Domain;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace AtelierEntertainment.Database
{
    public class CustomerRepository : ICustomerRepository
    {
        private static string _connString;
        public IConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AtelierEntertainment.Database.CustomerRepository"/> class.
        /// </summary>
        /// <param name="configuration">Configuration.</param>
        public CustomerRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connString = _configuration.GetSection("ConnectionStrings").GetSection("Default").Value;
        }

        /// <summary>
        /// Create the specified customer.
        /// </summary>
        /// <param name="customer">Customer.</param>
        public void Create(domain.Customer customer)
        {
            var dto = customer.ToDto();
            var conn = new SqlConnection(_connString);

            var cmd = conn.CreateCommand();

            cmd.CommandText = $"INSERT INTO dbo.Customers VALUES {dto.Id}, {dto.Country}";

            cmd.ExecuteNonQuery();
        }
    }
}
