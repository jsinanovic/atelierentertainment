﻿using System.Collections.Generic;

namespace AtelierEntertainment.Database
{
    public class Customer
    {
        public long? Id { get; set; }
        internal string Country;
    }

    /// <summary>
    /// Customer mappings.
    /// </summary>
    internal static class CustomerMappings
    {
        public static AtelierEntertainment.Domain.Customer ToDomain(this Customer customer) => new AtelierEntertainment.Domain.Customer
            (
            id: customer.Id,
            country: customer.Country
            );
        public static Customer ToDto(this AtelierEntertainment.Domain.Customer customer) => new Customer
        {
            Id = customer.Id,
            
        };
    }
}
