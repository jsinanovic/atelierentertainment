﻿using System.Collections.Generic;
using System.Linq;

namespace AtelierEntertainment.Database
{
    public class Order
    {
        public long Id { get; set; }
        public Customer Customer { get; set; }
        public long? CustomerId { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public decimal Total { get; set; }
    }

    public class OrderItem
    {
        public long? Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public long? OrderId { get; set; }
        public Order Order { get; set; }
    }

    /// <summary>
    /// Order mappings.
    /// </summary>
    internal static class OrderMappings
    {
        public static Order ToDto(this AtelierEntertainment.Domain.Order order) => new Order
        {
            Id = order.Id.GetValueOrDefault(),
            Total = order.Total,
            OrderItems = order.OrderItems != null ? order.OrderItems?.Select(x => x.ToDto()).ToList() :  new List<OrderItem>() { },
            Customer = order.Customer?.ToDto(),
            CustomerId = order.Customer.Id

        };

        public static AtelierEntertainment.Domain.Order ToDomain(this Order order)
        {
            return  new Domain.Order(
                order.Customer.ToDomain(),
                order.OrderItems?.Select(x => x.ToDomain()).ToList() ?? new List<AtelierEntertainment.Domain.OrderItem>() { },
                order.Total,
                order.CustomerId
                );
        }
        public static ICollection<AtelierEntertainment.Domain.Order> ToDomain(this ICollection<Order> orders) => orders.Select(x => x.ToDomain()).ToList();

        public static OrderItem ToDto(this Domain.OrderItem orderItem) => new OrderItem
        {
            Id = orderItem.Id,
            Code = orderItem.Code,
            Description = orderItem.Description,
            Price = orderItem.Price,
            OrderId = orderItem.OrderId
        };


        public static Domain.OrderItem ToDomain(this OrderItem orderItem, long? Id = null) => new Domain.OrderItem
            (orderId: Id ?? orderItem.OrderId, id: orderItem.Id)
        {
            Code = orderItem.Code,
            Description = orderItem.Description,
            Price = orderItem.Price,
            OrderId = orderItem.OrderId,
        };

        public static ICollection<Domain.OrderItem> ToDomain(this ICollection<OrderItem> orderItems) => orderItems.Select(x => x.ToDomain()).ToList();
    }
}