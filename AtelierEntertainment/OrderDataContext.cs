﻿
using System;
using AtelierEntertainment.Database.DataAccess;
using AtelierEntertainment.Database.Repository.Interfaces;
using System.Data.SqlClient;
using domain = AtelierEntertainment.Domain;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;

namespace AtelierEntertainment.Database
{
    public class OrderDataContext :  IOrderDataRepository
    {
        private static string _connString;
        public IConfiguration _configuration;

        public OrderDataContext(IConfiguration configuration)
        {
            _configuration = configuration;
            _connString = _configuration.GetSection("ConnectionStrings").GetSection("Default").Value;
        }

        /// <summary>
        /// Creates the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        public domain.Order CreateOrder(domain.Order order)
        {
            var dto = order.ToDto();
            var conn = new SqlConnection(_connString);
            conn.Open();
            var cmd = conn.CreateCommand();

            // cmd.CommandText = $"INSERT INTO dbo.Orders VALUES {dto.Id}, {dto.Customer.Id}, {dto.Total}";
            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
            IDParameter.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(IDParameter);

            cmd.CommandText = $"INSERT INTO dbo.Orders (CustomerId, Total) VALUES( {dto.Customer.Id}, {dto.Total}) SET @ID = SCOPE_IDENTITY()";


            cmd.ExecuteNonQuery();
            int orderId = (int)cmd.Parameters["@ID"].Value;

            foreach (var item in order.OrderItems)
            {
                cmd = conn.CreateCommand();

                cmd.CommandText = $"INSERT INTO dbo.OrderItems (Code, Description, Price, OrderId) VALUES ('{item.Code}', '{item.Description}', '{item.Price}',{orderId})";

                cmd.ExecuteNonQuery();
            }
            return order;
        }

        /// <summary>
        /// Loads the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="id">Identifier.</param>
        public static domain.Order LoadOrder(long id)
        {
            var conn = new SqlConnection(_connString);
            conn.Open();

            var cmd = conn.CreateCommand();
            cmd.CommandText = $"SELECT * FROM dbo.Orders WHERE Id = {id}";

            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            var result = new Order { };

            result.Id = id;
            result.Total = reader.GetDecimal(2);
            result.OrderItems = new List<OrderItem>();
            result.Customer = new Customer { };
            result.CustomerId = reader.GetInt64(1);

            cmd = conn.CreateCommand();

            cmd.CommandText = $"SELECT * FROM dbo.OrderItems WHERE OrderId = {id}";
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                result.OrderItems.Add(new OrderItem { Code = reader.GetString(1), Description = reader.GetString(2), Price = reader.GetFloat(3) });
            }
            return result.ToDomain();
        }

        public ICollection<domain.Order> GetByCustomerId(long customerId)
        {
            var conn = new SqlConnection(_connString);
            var cmd = conn.CreateCommand();
            conn.Open();
            cmd.CommandText = $"SELECT ord.*, cust.Country, oi.* FROM dbo.Orders as ord INNER JOIN Customers cust ON ord.CustomerId = cust.Id INNER JOIN OrderItems oi ON ord.Id = oi.OrderId Where ord.CustomerId = {customerId}";
            SqlDataReader reader = cmd.ExecuteReader();

            var result = new List<Order> { };
            while (reader.Read())
            {
                result.Add(new Order { Id = reader.GetInt64(0), Total = reader.GetDecimal(2), Customer = new Customer { Country = reader.GetString(3), Id = reader.GetInt64(1) }, OrderItems = new List<OrderItem> { new OrderItem { Id = reader.GetInt64(4), Code = reader.GetString(5), Description = reader.GetString(6), Price = reader.GetFloat(7), OrderId = reader.GetInt64(8) } }, CustomerId = reader.GetInt64(1) });
            }
            return result.ToDomain();
        }
    }
}
