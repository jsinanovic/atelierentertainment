﻿
using AtelierEntertainment.Database.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AtelierEntertainment.Database
{
    public static class StartupRepositoryServices
    {
        /// <summary>
        /// Adds the repository services.
        /// </summary>
        /// <param name="services">Services.</param>
        /// <param name="configuration">Configuration.</param>
        public static void AddRepositoryServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IOrderDataRepository, OrderDataContext>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
        }
    }
}
