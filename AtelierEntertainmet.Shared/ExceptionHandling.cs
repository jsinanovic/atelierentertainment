﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.Security.Authentication;
using AtelierEntertainmet.Shared.Exceptions;

namespace AtelierEntertainmet.Shared
{
    public class ExceptionHandling
    {
        // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandling> _logger;
        private readonly IConfiguration _configuration;

        public ExceptionHandling(ILogger<ExceptionHandling> logger, RequestDelegate next, IConfiguration configuration)
        {
            _next = next;
            _logger = logger;
            _configuration = configuration;
        }

        /// <summary>
        /// Invoke the specified httpContext.
        /// </summary>
        /// <returns>The invoke.</returns>
        /// <param name="httpContext">Http context.</param>
        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (NotFoundException ex)
            {
                if (!httpContext.Response.HasStarted)
                {
                    // httpContext.Response.Clear();
                    httpContext.Response.StatusCode = StatusCodes.Status404NotFound;

                    AddCorsHeaders(httpContext);

                    var exception = JsonConvert.SerializeObject(new { ex.Message });

                    await httpContext.Response.WriteAsync(exception);
                }
            }
            // Catch wrapped validation exceptions in Automapper exceptions
            catch (Exception ex) when (ex is ValidationException || ex.GetBaseException() is ValidationException)
            {
                ValidationException exception;
                if (ex.GetType() != typeof(ValidationException))
                {
                    exception = ex.GetBaseException() as ValidationException;
                }
                else
                {
                    exception = ex as ValidationException;
                }
                if (!httpContext.Response.HasStarted)
                {
                    // httpContext.Response.Clear();
                    httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;

                    AddCorsHeaders(httpContext);

                    if (exception.Errors.Count <= 0)
                    {
                        var errors = JsonConvert.SerializeObject(new { exception.Message });
                        await httpContext.Response.WriteAsync(errors);
                    }
                    else
                    {
                        var errors = JsonConvert.SerializeObject(exception.Errors);
                        await httpContext.Response.WriteAsync(errors);
                    }
                }
            }
            catch (AtelierEntertainmet.Shared.Exceptions.AuthenticationException ex)
            {
                if (!httpContext.Response.HasStarted)
                {
                    // httpContext.Response.Clear();
                    httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;

                    AddCorsHeaders(httpContext);

                    var exception = JsonConvert.SerializeObject(new { ex.Message, ex.Code });

                    if (ex.InnerException != null)
                    {
                        _logger.LogError(ex.InnerException, $"Unhandled error occured with message: {ex.InnerException.Message}");
                    }

                    await httpContext.Response.WriteAsync(exception);
                }
            }
            catch (ForbiddenException ex)
            {
                if (!httpContext.Response.HasStarted)
                {
                    //httpContext.Response.Clear();
                    httpContext.Response.StatusCode = StatusCodes.Status403Forbidden;

                    AddCorsHeaders(httpContext);

                    var exception = JsonConvert.SerializeObject(new { ex.Message, ex.Code });

                    await httpContext.Response.WriteAsync(exception);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                if (!httpContext.Response.HasStarted)
                {
                    //httpContext.Response.Clear();
                    httpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;

                    AddCorsHeaders(httpContext);

                    var exception = JsonConvert.SerializeObject(new { ex.Message });

                    await httpContext.Response.WriteAsync(exception);
                }
            }
            catch (NotImplementedException ex)
            {
                if (!httpContext.Response.HasStarted)
                {
                    // httpContext.Response.Clear();
                    httpContext.Response.StatusCode = StatusCodes.Status501NotImplemented;

                    AddCorsHeaders(httpContext);

                    var exception = JsonConvert.SerializeObject(new { ex.Message });

                    await httpContext.Response.WriteAsync(exception);
                }
            }
            catch (Exception ex) when (ex.GetType().Name == "SqlException")
            {
                if (!httpContext.Response.HasStarted)
                {
                    var message = ex.Message; //
                    //httpContext.Response.Clear();
                    if (ex.Message.StartsWith("Cannot open connection") && ex.Message.Contains("Ws_"))
                    {
                        httpContext.Response.StatusCode = StatusCodes.Status404NotFound;
                        message = "NOT_FOUND"; // consider refactoring
                    }
                    else
                    {
                        httpContext.Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
                        _logger.LogError(ex.InnerException, $"Unhandled error occured with message: {ex.Message}");
                        message = "Cannot connect to given workspace";
                    }

                    AddCorsHeaders(httpContext);

                    var exception = JsonConvert.SerializeObject(new { Message = message });

                    await httpContext.Response.WriteAsync(exception);
                }
            }
            catch (Exception ex) when (ex.GetType().Name == "DbUpdateException" || ex.GetType().Name == "DbUpdateConcurrencyException")
            {
                if (!httpContext.Response.HasStarted)
                {
                    //httpContext.Response.Clear();
                    httpContext.Response.StatusCode = StatusCodes.Status404NotFound;

                    AddCorsHeaders(httpContext);

                    var exception = JsonConvert.SerializeObject(new { ex.Message });

                    await httpContext.Response.WriteAsync(exception);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled error occured with message: {ex.Message}");
                if (!httpContext.Response.HasStarted)
                {
                    // httpContext.Response.Clear();
                    httpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;

                    AddCorsHeaders(httpContext);

                    var exception = JsonConvert.SerializeObject(new { ex.Message });

                    await httpContext.Response.WriteAsync(exception);
                }
            }
        }

        private void AddCorsHeaders(HttpContext httpContext)
        {
            httpContext.Response.Headers.Add("Access-Control-Allow-Origin", _configuration["AllowedCors"].Split(";"));
            httpContext.Response.Headers.Add("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, PATCH, DELETE");
            httpContext.Response.Headers.Add("Access-Control-Allow-Headers", "X-PINGOTHER, Content-Type, Authorization");
            httpContext.Response.Headers.Add("Content-Type", "application/json");
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ExceptionHandlingMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionHandling(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandling>();
        }
    }
}
