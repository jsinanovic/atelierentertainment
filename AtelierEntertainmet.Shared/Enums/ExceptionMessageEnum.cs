﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtelierEntertainmet.Shared.Enums
{
    public enum ExceptionMessageEnum
    {
        NotValid,
        InvalidRequest
    }
}
