﻿using AtelierEntertainmet.Shared.Enums;
using System;

namespace AtelierEntertainmet.Shared.Exceptions
{
    public class AuthenticationException : Exception
    {
        public AuthenticationException(ExceptionMessageEnum code, Exception innerException = null) : base(
            code.ToString(), innerException)
        {
            Code = code;
        }

        public ExceptionMessageEnum Code { get; }

    }
}
