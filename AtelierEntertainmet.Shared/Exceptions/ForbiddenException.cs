﻿using AtelierEntertainmet.Shared.Enums;
using System;

namespace AtelierEntertainmet.Shared.Exceptions
{
    public class ForbiddenException : Exception
    {
        public long Code { get; }
        public ForbiddenException()
        {
        }

        public ForbiddenException(string message) : base(message)
        {
        }

        public ForbiddenException(ExceptionMessageEnum error) : base(error.ToString())
        {
            Code = (int)error;
        }
    }
}
