﻿using AtelierEntertainment.Api.Controllers;
using AtelierEntertainment.Api.Services;
using AtelierEntertainment.Database.Repository.Interfaces;
using AtelierEntertainment.Domain;
using Microsoft.Extensions.Configuration;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Dtos = AtelierEntertainment.Api.Dtos;

namespace AtelierEntertainment.Tests
{
    public class OrderTests : BaseTest
    {
        [Fact]
        public async Task CreateCompany()
        {
            var orderRepository = new Mock<IOrderDataRepository>();
            var companyService = new Mock<OrderService>(orderRepository.Object);
            var configuration = new Mock<IConfiguration>();
            var orderController = new OrderController(companyService.Object, configuration.Object);

            List<Dtos.OrderItem> items = new List<Dtos.OrderItem>();
            items.Add(new Dtos.OrderItem
            {
                Id = 57,
                Code = "34h5sha",
                Description = "test description",
                Price = 10,
                OrderId = 1,
            });

            var contract = new Dtos.Order
            {
                Customer = new Dtos.Customer { Id = 1 },
                CustomerId = 1,
                Id = 1,
                Total = 10,
                OrderItems = items
            };

            var order = new  Order(null, new List<OrderItem>() { }, 10, 1);
            orderRepository.Setup(x => x.CreateOrder(It.IsAny<Order>())).Returns(order);

            var result = await HttpResponseExtractor(() => orderController.CreateOrder(contract));
      
            Assert.Equal(result.Total, order.Total); 
        }

        /// <summary>
        /// Gets the by cusomer identifier.
        /// </summary>
        /// <returns>The by cusomer identifier.</returns>
        [Fact]
        public async Task GetByCusomerId()
        {
            var orderRepository = new Mock<IOrderDataRepository>();
            var orderService = new Mock<OrderService>(orderRepository.Object);
            var configuration = new Mock<IConfiguration>();
            var orderController = new OrderController(orderService.Object, configuration.Object);

            List<Dtos.OrderItem> items = new List<Dtos.OrderItem>();
            items.Add(new Dtos.OrderItem
            {
                Id = 57,
                Code = "34h5sha",
                Description = "test description",
                Price = 10,
                OrderId = 1,
            });

            List<Domain.OrderItem> item = new List<Domain.OrderItem>();
            item.Add(new Domain.OrderItem(orderId: 1, id: 1));

            List<Order> orders = new List<Order>();
            orders.Add(new Order(null, item, 10, 1));

            orderRepository.Setup(x => x.GetByCustomerId(1)).Returns(orders);
            var result = HttpResponseExtractor(() => orderController.GetByCustomerId(1));

            Assert.NotNull(result);
        }
    }
}
