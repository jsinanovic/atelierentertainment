﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AtelierEntertainment.Tests
{
    public abstract class BaseTest
    {
        protected async Task<T> HttpResponseExtractor<T>(Func<Task<ActionResult<T>>> action) where T : class
        {
            var response = await action();
            if (response != null && response.Result != null && response.Result is ObjectResult obj)
            {
                return obj.Value as T;
               // return response.Value;
            }
            return response.Value;
        }
    }
}
