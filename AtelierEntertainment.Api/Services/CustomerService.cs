﻿using AtelierEntertainment.Api.Dtos;
using AtelierEntertainment.Database.Repository.Interfaces;
using System;
using System.Linq;
using domain = AtelierEntertainment.Domain;

namespace AtelierEntertainment.Api.Services
{
    /// <summary>
    /// Customer service.
    /// </summary>
    public class CustomerService
    {
        private readonly ICustomerRepository _customerDataRepository;

        public CustomerService(ICustomerRepository customerDataRepository)
        {
            _customerDataRepository = customerDataRepository;
        }

        public void CreateOrder(domain.Customer customer)
        {
            _customerDataRepository.Create(customer);
        }
    }
}
