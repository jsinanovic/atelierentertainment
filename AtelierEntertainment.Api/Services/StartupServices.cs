﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AtelierEntertainment.Api.Services
{
    public static class StartupServices
    {
        /// <summary>
        /// Adds the API services.
        /// </summary>
        /// <param name="services">Services.</param>
        /// <param name="configuration">Configuration.</param>
        public static void AddApiServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<OrderService>();
            services.AddScoped<CustomerService>();
        }
    }
}
