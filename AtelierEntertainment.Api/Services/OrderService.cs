﻿using AtelierEntertainment.Api.Dtos;
using AtelierEntertainment.Database;
using AtelierEntertainment.Database.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain = AtelierEntertainment.Domain;

namespace AtelierEntertainment.Api.Services
{
    /// <summary>
    /// Order service.
    /// </summary>
    public class OrderService
    {
        private readonly IOrderDataRepository _orderDataRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AtelierEntertainment.Api.Services.OrderService"/> class.
        /// </summary>
        /// <param name="orderDataRepository">Order data repository.</param>
        public OrderService(IOrderDataRepository orderDataRepository)
        {
            _orderDataRepository = orderDataRepository;
        }

        /// <summary>
        /// Creates the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        public domain.Order CreateOrder(domain.Order order)
        {
            var response =  _orderDataRepository.CreateOrder(order);
            return response;
        }

        /// <summary>
        /// Gets the by customer identifier.
        /// </summary>
        /// <returns>The by customer identifier.</returns>
        /// <param name="customerId">Customer identifier.</param>
        public ICollection<domain.Order> GetByCustomerId(long customerId)
        {
            var orders = _orderDataRepository.GetByCustomerId(customerId);
            return orders;
        }

        /// <summary>
        /// Views the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="id">Identifier.</param>
        public static domain.Order ViewOrder(long id)
        {
            return  OrderDataContext.LoadOrder(id);
        }
    }
}
