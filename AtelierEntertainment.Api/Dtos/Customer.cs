﻿using System.Collections.Generic;
using System.Linq;

namespace AtelierEntertainment.Api.Dtos
{
    public class Customer
    {
        public long? Id { get; set; }

        internal string Country;
    }

    /// <summary>
    /// Customer mappings.
    /// </summary>
    internal static class CustomerMappings
    {
        public static AtelierEntertainment.Domain.Customer ToDomain(this Customer customer) => new AtelierEntertainment.Domain.Customer
            (
                id: customer.Id,
                country: customer.Country == null ? "AU" : "NZ"
            );
        public static Customer ToDto(this AtelierEntertainment.Domain.Customer customer) => new Customer
        {
            Id = customer.Id,
            Country = "AU"

        };
    }
}
