﻿using AtelierEntertainment.Api.Dtos;
using System.Collections.Generic;
using System.Linq;

namespace AtelierEntertainment.Api.Dtos
{
    public class Order
    {
        public long? Id { get; set; }
        public Customer Customer { get; set; }
        public long? CustomerId { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public decimal Total { get; set; }
    }

    public class OrderItem
    {
        public long? Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public Order Order { get; set; }
        public long? OrderId { get; set; }
    }
}

/// <summary>
/// Order mappings.
/// </summary>
internal static class OrderMappings
{
    public static AtelierEntertainment.Domain.Order ToDomain(this Order order) => new AtelierEntertainment.Domain.Order(
        orderItems: order.OrderItems?.Select(x => x.ToDomain()).ToList() ?? new List<AtelierEntertainment.Domain.OrderItem>() { },
        customer: order.Customer?.ToDomain()
        );
    public static Order ToDto(this AtelierEntertainment.Domain.Order order) => new Order
    {
        Id = order.Id,
        OrderItems = order.OrderItems != null ? order.OrderItems?.Select(x => x.ToDto()).ToList() : new List<AtelierEntertainment.Api.Dtos.OrderItem>() { },
        Total = order.Total,
        Customer = order.Customer != null ? order.Customer?.ToDto() :  new AtelierEntertainment.Api.Dtos.Customer() { },
    };

    public static AtelierEntertainment.Domain.OrderItem ToDomain(this OrderItem orderItem) => new AtelierEntertainment.Domain.OrderItem(orderItem.OrderId, orderItem.Id)
    {
        Code = orderItem.Code,
        Description = orderItem.Description,
        Price = orderItem.Price,
        OrderId = orderItem.OrderId
    };

    public static OrderItem ToDto(this AtelierEntertainment.Domain.OrderItem orderItem) => new OrderItem
    {
        Id = orderItem.Id,
        OrderId = orderItem.OrderId,
        Code = orderItem.Code,
        Description = orderItem.Description,
        Price = orderItem.Price
    };

    public static ICollection<AtelierEntertainment.Domain.OrderItem> ToDomain(this ICollection<OrderItem> orderItems) => orderItems.Select(x => x.ToDomain()).ToList();

    public static ICollection<AtelierEntertainment.Domain.Order> ToDomain(this ICollection<Order> orders) => orders.Select(x => x.ToDomain()).ToList();
}