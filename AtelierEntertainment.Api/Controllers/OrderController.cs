﻿using AtelierEntertainment.Api.Dtos;
using AtelierEntertainment.Api.Services;
using AtelierEntertainmet.Shared.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AtelierEntertainment.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : BaseController
    {
        private readonly OrderService _orderService;

        public OrderController(OrderService orderService, IConfiguration configuration) : base(configuration)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Creates the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="order">Order.</param>
        [HttpPost]
        public async Task<ActionResult<Order>> CreateOrder(Order order)
        {
            if (order.Id.HasValue)
            {
                order.Id = null;
            }

            if (order.OrderItems == null || order.OrderItems.Count == 0 || !order.CustomerId.HasValue)
                throw new ForbiddenException("Order must contain orderItems and Customer");

            var response = _orderService.CreateOrder(order.ToDomain());

            return response.ToDto();
        }

        /// <summary>
        /// Gets the by customer identifier.
        /// </summary>
        /// <returns>The by customer identifier.</returns>
        /// <param name="customerId">Customer identifier.</param>
        [HttpGet("orderByCustomer/{customerId}")]
        public async Task<ActionResult<ICollection<Order>>> GetByCustomerId(long customerId)
        {
            var orders = _orderService.GetByCustomerId(customerId);
            return Ok(orders);
        }

        /// <summary>
        /// Views the order.
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="Id">Identifier.</param>
        [HttpGet("{Id}")]
        public async Task<ActionResult<ICollection<Order>>> ViewOrder(long Id)
        {
            var orders = OrderService.ViewOrder(Id);
            return Ok(orders);
        }
    }
}