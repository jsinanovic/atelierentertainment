﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AtelierEntertainment.Api.Controllers
{/// <summary>
 /// Set of basic properties/functions used across all controllers
 /// </summary>
    public abstract class BaseController : ControllerBase
    {
        /// <summary>
        /// Configuration from environment/file
        /// </summary>
        protected readonly IConfiguration _configuration;

        /// <summary>
        /// Required dependencies for all controllers
        /// </summary>
        /// <param name="configuration"></param>
        public BaseController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
    }
}
