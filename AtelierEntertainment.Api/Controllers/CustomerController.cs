﻿using AtelierEntertainment.Api.Dtos;
using AtelierEntertainment.Api.Services;
using AtelierEntertainmet.Shared.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace AtelierEntertainment.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : BaseController
    {
        private readonly CustomerService _customerService;

        public CustomerController(CustomerService customerService, IConfiguration configuration) : base(configuration)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Create the specified customer.
        /// </summary>
        /// <returns>The create.</returns>
        /// <param name="customer">Customer.</param>
        [HttpPost]
        public async Task<ActionResult> Create(Customer customer)
        {
            if (customer.Id.HasValue)
            {
                customer.Id = null;
            }

            _customerService.CreateOrder(customer.ToDomain());
            return Ok();
        }
    }
}
