﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AtelierEntertainment.Domain
{
    public class Order
    {
        private const decimal AU_TAX = 1.1m;
        private const decimal UK_TAX = 1.2m;

        public long? Id { get; private set; }

        public Customer Customer { get; set; }
        public long? CustomerId { get; set; }
        public IReadOnlyCollection<OrderItem> OrderItems => _orderItems.AsReadOnly();
        private List<OrderItem> _orderItems;
        public decimal Total { get; private set; }

        public Order(Customer customer = null, List<OrderItem> orderItems = null, decimal total = 0,  long? customerId = null)
        {
            Customer = customer;
            _orderItems = orderItems;
            Total = total;
            CustomerId = customerId;
        }

        public Order(Customer customer = null, List<OrderItem> orderItems = null)
        {
            Customer = customer;
            if (orderItems == null)
            {
                _orderItems = new List<OrderItem>();
            }
            else
            {
                _orderItems = orderItems;
            }
            CalculateTotal();
        }

        public void AddItem(OrderItem item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));

            _orderItems.Add(item);

            CalculateTotal();
        }

        public void AddItems(IEnumerable<OrderItem> items)
        {
            if (items == null) throw new ArgumentNullException(nameof(items));
            if (items.Any(x => x == null)) throw new ArgumentNullException(nameof(items));

            _orderItems.AddRange(items);
            CalculateTotal();
        }

        public void RemoveItem(int index)
        {
            if (index < 0) throw new ArgumentOutOfRangeException(nameof(index));
            if (index > _orderItems.Count) throw new ArgumentOutOfRangeException(nameof(index));

            _orderItems.RemoveAt(index);

            CalculateTotal();
        }

        private void CalculateTotal()
        {
            Total = Convert.ToDecimal(OrderItems.Sum(x => x.Price));

            if (Customer == null) return;

            if (Customer.Country == "AU")
            {
                Total *= AU_TAX;
            }
            else if (Customer.Country == "UK")
            {
                Total *= UK_TAX;
            }
        }
    }

    public class OrderItem
    {
        public long? Id { get; }
        public string Code { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public long? OrderId { get; set; }

        public OrderItem(long? orderId, long? id = null)
        {
            Id = id;
            // if order is new it does not have ID but it can contain items
            if (orderId.HasValue)
            {
                OrderId = orderId.Value;
            }
        }
    }
}