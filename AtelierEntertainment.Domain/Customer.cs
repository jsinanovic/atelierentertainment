﻿using System.Collections.Generic;

namespace AtelierEntertainment.Domain
{
    public class Customer
    {
        public long? Id { get; }
        internal string Country;

        public Customer(long? id, string country = null)
        {
            Id = id;
            Country = country;
        }
    }
}
